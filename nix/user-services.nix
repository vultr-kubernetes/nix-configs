{ config, pkgs, ... }:

{
  code-server = {
    Unit = {
      Description = "Code Server Web IDE";
      After = [
        "network.target"
      ];
    };

    Service = {
      Type = "simple";
      ExecStart = "${pkgs.code-server}/bin/code-server";
      Restart = "on-failure";
    };

    Install = {
      WantedBy = [
        "multi-user.target"
      ];
    };
  };
}
